### Lab on pointillism
This is a standalone version of the Pointillism project,
out of Eclipse environment.

#### To build the project
Go to Debug folder and type:
```make```

#### To execute the executable
Go back one level:
```cd ..```

then launch the executable "PointillismEmpty" which is in the Debug folder:
```./Debug/PointillismEmpty```

or change the path to the images in the source code 
