#include "cdf.h"

#include <cstdlib>
#include <algorithm>

CDF::CDF(const Image& img)
{
    std::vector<float> value_sum_of_cdf; // store the sum of each line, by channel
    value_sum_of_cdf.resize(img.getHeight());

    /******* compute cdf lines *******/
    _cdf_lines.resize( img.getHeight() );

    for(int i=0 ; i<img.getHeight() ; ++i) // for each line
    {
        // fill cdf lines
        float sum_values = 0;
        for(int j=0 ; j<img.getWidth() ; ++j) {
            float v = img.getPixel(j, i, 0);
            _cdf_lines[i].push_back( sum_values + v );
            sum_values += v;
        }

        // normalize cdf lines
        if(sum_values > 0) {
            for(uint j=0 ; j<_cdf_lines[i].size(); ++j)
                _cdf_lines[i][j] /= sum_values;
        }

        // save sum value
        value_sum_of_cdf[i] = sum_values;
    }

    /******* compute cdf columns *******/
    // fill cdf columns
    float sum_values = 0;
    for(int i=0 ; i<img.getHeight() ; ++i) // for each line
    {
        float v = value_sum_of_cdf[i];
        _cdf_columns.push_back( sum_values + v );
        sum_values += v;
    }

    // normalize cdf columns
    if(sum_values > 0)
        for(uint i=0 ; i<_cdf_columns.size() ; ++i)
            _cdf_columns[i] /= sum_values;
}

int CDF::inverseX(float r, float y) {
   return std::lower_bound( _cdf_lines[y].begin(),  _cdf_lines[y].end(), r) - _cdf_lines[y].begin();
}

int CDF::inverseY(float r) {
   return std::lower_bound( _cdf_columns.begin(),  _cdf_columns.end(), r) - _cdf_columns.begin();
}

