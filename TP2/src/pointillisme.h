#ifndef POINTILLISME_H
#define POINTILLISME_H

#include "cdf.h"
#include "random.h"

class Pointillisme
{
public:
    Pointillisme(unsigned int n=10000);
    virtual void process(Image& img) const;
    virtual void processed(const Image& in, Image& out) const;
    void setNofSamples(unsigned int n) { _nof_samples = n; }

protected:
    void initOutputImage(const Image& in, Image &out) const;
    void computeSumValues(const Image& img) const;

    void sequentialSampling(Image &img, CDF &cdf) const;
    void parallelizedSampling(Image &img, CDF &cdf) const;
    void twoStepSampling(Image &img, CDF &cdf) const;
    void twoStepParallelizedSampling(Image &img, CDF &cdf) const;

protected:
    unsigned int _nof_samples;
    mutable float _image_sum_values;
};

#endif // POINTILLISME_H
