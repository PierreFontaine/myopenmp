#ifndef IMAGE_H
#define IMAGE_H

#include <vector>
#include "layer.h"


class Image
{
public:
    enum ImageType {
        GRAY_SCALE,
        RGB
    };

    Image();
    Image(const Image& img);
    ~Image() {}

    void resize(int width, int height, int channel);

    float getPixel(int i, int j, int c) const;
    float& getPixelRef(int i, int j, int c)
    {
        return _layers[c].getPixelRef(i,j);
    }

    void setPixel(int i, int j, int c, float value);

    int getWidth() const { return _width; }
    int getHeight() const { return _height; }

    int getNofLayer() const { return _layers.size(); }

    void debug();

    ImageType getType() const;
    void setType(ImageType t);

private:
    int _width, _height;
    std::vector<Layer> _layers;
    ImageType type;

};

#endif // IMAGE_H
