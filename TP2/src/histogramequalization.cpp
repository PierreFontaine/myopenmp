#include "histogramequalization.h"

#include <iostream>
#include <limits>
#include <omp.h>
#include <cmath>

using namespace std;

/**
 * @brief Egalise l'image passé en param
 * @param img Image a egaliser
 * @param min pixel le plus bas
 * @param max pixel le plus haut
 */
void equalizeSequential(Image &img, float min, float max) {
    int h = img.getHeight();
    int w = img.getWidth();

    for (int i = 0; i < h; i++) {
    	for (int j = 0; j < w; j++) {
			float pixel = img.getPixel(j,i,0);
			float newPixel = (pixel - min) / (max - min);
			img.setPixel(j,i,0,newPixel);
    	}
    }
}

/**
 * @brief Egalise l'image passé en param de facon parallele
 * @param img img a egaliser
 * @param min pixel le plus bas
 * @param max pixel le plus haut
 */
void equalizeParallelized(Image &img, float min, float max) {
	int h = img.getHeight();
	int w = img.getWidth();
	int i,j; //Iterateur

	float pixel, newPixel;

	#pragma omp parallel default(none) shared(h,w, min, max, img) private(i,j, pixel, newPixel)
	{
		#pragma omp for collapse(2)
		for (i = 0; i < h; i++) {
			for (j = 0; j < w; j++) {
				pixel = img.getPixel(j,i,0);
				newPixel = (pixel - min) / (max - min);
				img.setPixel(j,i,0,newPixel);
			}
		}
	}
}

/**
 * @brief Permet de trouver le pixel min et le pixel max de facon parallele
 * @param img
 * @param min retour par ref du pixel min
 * @param max retour par ref du pixel max
 */
void minMaxSearchSequential(const Image &img, float &min, float &max) {
    // init min and max values
    min = std::numeric_limits<float>::max();
    max = std::numeric_limits<float>::min();

    // for each pixel
    for(int j=0 ; j<img.getHeight() ; ++j)
        for(int i=0 ; i<img.getWidth() ; ++i)
        {
            // update min and max values
            float v = img.getPixel(i, j, 0);
            if( min > v ) min = v;
            if( max < v ) max = v;
        }
}

void minMaxSearchReduction(const Image &img, float &min, float &max) {

	min = std::numeric_limits<float>::max();
	max = std::numeric_limits<float>::min();
	int i,j;
	float v;
	int h = img.getHeight();
	int w = img.getWidth();

	#pragma omp parallel default(none) shared(img,h, w, min ,max) private(v,j,i)
	{
		#pragma omp for collapse(2) reduction(min:min) reduction(max:max)
		for(j=0 ; j<h ; ++j) {
			for(i=0 ; i<w ; ++i){
				// update min and max values
				v = img.getPixel(i, j, 0);
				if( min > v )
					min = v;
				if( max < v )
					max = v;
			}
		}
	}
}