#ifndef CDF_H
#define CDF_H

#include "image.h"

// A 2D CDF
class CDF {
	public:
	    // Construct a new 2D CDF of an Image
	    CDF(const Image& img);

	    // Return the index of a line depanding on the random number r
	    // r must be in [0, 1]
	    int inverseY(float r);

	    // Return the index of a column depanding on the random number r and the line number y
	    // r must be in [0, 1]
	    // y is the number of a line
	    int inverseX(float r, float y);

	protected:
	    std::vector<std::vector<float> > _cdf_lines;
	    std::vector<float> _cdf_columns;
};

#endif // CDF_H
