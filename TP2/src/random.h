#ifndef RANDOM_H
#define RANDOM_H

#include <random>

// A random generator

class Random
{
public:
    // Construct a new random generator. The random value will be in the range [a, b]
    Random(double a, double b) : _gen(_rd()), _dis(a, b) {}

    // Generate a random number
    double generate() { return _dis(_gen); }

protected:
    std::random_device _rd;
    std::mt19937 _gen;
    std::uniform_real_distribution<> _dis;
};

#endif // RANDOM_H
