#include "layer.h"

#include <iostream>
#include <assert.h>

Layer::Layer()
{
}

Layer::Layer(const Layer &l)
    :_width(l._width), _height(l._height), _pixels(l._pixels)
{

}

void Layer::resize(int w, int h)
{
    _width = w;
    _height = h;
    _pixels.resize(_width*_height);
}

float Layer::getPixel(int i, int j) const {
    int index = j * _width + i;
    return _pixels[index];
}

void Layer::setPixel(int i, int j, float value) {
    unsigned int index = j * _width + i;
    assert(index < _pixels.size());
    _pixels[index] = value;
}

void Layer::debug()
{
    for(int j=0; j<_height; j++) {
        for(int i=0; i<_width; i++) {
            int index = j * _width + i;
            std::cout << _pixels[index] << " ";
        }
        std::cout << std::endl;
    }
}
