#include "utils.h"

double time()
{
    return omp_get_wtime();
}

double elapsed(const double start, const double end)
{
    double elapsed_time = end-start;
    return elapsed_time;
}

