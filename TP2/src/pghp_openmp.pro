TEMPLATE = app
CONFIG += console
CONFIG += c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    image.cpp \
    layer.cpp \
    imageio.cpp \
    pointillisme.cpp \
    utils.cpp \
    histogramequalization.cpp \
    cdf.cpp

HEADERS += \
    image.h \
    layer.h \
    imageio.h \
    pointillisme.h \
    utils.h \
    random.h \
    histogramequalization.h \
    cdf.h

LIBS += -lgomp

QMAKE_CXXFLAGS += -fopenmp
QMAKE_LFLAGS += -fopenmp

OTHER_FILES += \
    test.pgm

