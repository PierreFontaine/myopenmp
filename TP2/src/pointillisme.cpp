#include "utils.h"
#include "pointillisme.h"
#include "image.h"

#include <iostream>
#include <cmath>
#include <cstdlib>
#include <omp.h>
#include <map>
#include <stdlib.h>
#include <cstring>
using namespace std;

Pointillisme::Pointillisme(unsigned int n)
    : _nof_samples(n)
{

}

void Pointillisme::process(Image &img) const {
    Image out;
    processed(img, out);
    img = out;
}

void Pointillisme::processed(const Image &in, Image &out) const {
    double start, end;


    // compute cdf
    std::cout << "[Pointillisme] Compute CDF... " << std::flush; start = time();
    CDF cdf(in);
    std::cout << elapsed(start, end=time()) << "s" << std::endl;

    // fill out with white
    std::cout << "[Pointillisme] Init image... " << std::flush; start = time();
    initOutputImage(in, out);
    std::cout << elapsed(start, end=time()) << "s" << std::endl;

    // sum all values
    std::cout << "[Pointillisme] Compute sum of values... " << std::flush; start = time();
    computeSumValues(in);
    std::cout << elapsed(start, end=time()) << "s" << std::endl;

    // generate samples sequential version
    Image img1 = out;
    std::cout << "[Pointillisme] Generate samples naive sequential version (please wait... typically 20 seconds)... " << std::flush; start = time();
    sequentialSampling(img1,cdf);
    std::cout << elapsed(start, end=time()) << "s" << std::endl;

    // generate samples parallelized version
    Image img2 = out;
    std::cout << "[Pointillisme] Generate samples naive parallelized version... " << std::flush; start = time();
    parallelizedSampling(img2,cdf);
    std::cout << elapsed(start, end=time()) << "s" << std::endl;

    // generate samples parallelized version
    Image img3 = out;
    std::cout << "[Pointillisme] Generate samples two step sequential version... " << std::flush; start = time();
    twoStepSampling(img3,cdf);
    std::cout << elapsed(start, end=time()) << "s" << std::endl;

    // generate samples parallelized version
    Image img4 = out;
    std::cout << "[Pointillisme] Generate samples two step parallelized version... " << std::flush; start = time();
    twoStepParallelizedSampling(img4,cdf);
    std::cout << elapsed(start, end=time()) << "s" << std::endl;

    // out will be save. You can change it
    out = img1;
}

void Pointillisme::initOutputImage(const Image &in, Image &out) const
{
    out.resize(in.getWidth(), in.getHeight(), in.getNofLayer());
    for(int j=0 ; j<out.getHeight() ; ++j)
        for(int i=0 ; i<out.getWidth() ; ++i)
            out.setPixel(i, j, 0, 0);
}

void Pointillisme::computeSumValues(const Image &img) const
{
    _image_sum_values = 0;
    for(int j=0 ; j<img.getHeight() ; ++j)
        for(int i=0 ; i<img.getWidth() ; ++i)
            _image_sum_values += img.getPixel(i, j, 0);
}

/**
 * @brief
 * @param img
 * @param cdf
 */
void Pointillisme::sequentialSampling(Image& img, CDF &cdf) const {
	// init random
	Random gen(0., 1.);
	// for each sample
	for(uint i=0 ; i<_nof_samples ; ++i) {
		// generale x and y coordinates
		int x, y;
		float rand_x = gen.generate();
		float rand_y = gen.generate();
		y = cdf.inverseY(rand_y);
		x = cdf.inverseX(rand_x,y);
		// update pixel
		float v = img.getPixel(x, y, 0) + _image_sum_values / _nof_samples;
		img.setPixel(x, y, 0, std::max(std::min(v, 1.f), 0.f));
	}
}

/**
 * @brief
 * @param img
 * @param cdf
 */
void Pointillisme::parallelizedSampling(Image &img, CDF &cdf) const {
	uint i;
	int x, y;
	float rand_x, rand_y, v;

	#pragma omp parallel default(none) shared(cdf, img) private(rand_x, rand_y, i, y, x, v)
	{
		Random gen(0., 1.);
		#pragma omp for
		for(i=0 ; i < _nof_samples ; ++i) {
			rand_x = gen.generate();
			rand_y = gen.generate();

			y = cdf.inverseY(rand_y);
			x = cdf.inverseX(rand_x,y);
			v = img.getPixel(x, y, 0) + _image_sum_values / _nof_samples;
			#pragma omp critical
			img.setPixel(x, y, 0, std::max(std::min(v, 1.f), 0.f));
		}
	}
}

void Pointillisme::twoStepSampling(Image &img, CDF &cdf) const {
	//Generateur d'alea entre 0 et 1
	Random gen(0., 1.);
	float rand_x, rand_y, v;
	int i, x, l, y;
	int *sample_save = (int*) malloc(img.getHeight() * sizeof(int));
	memset(sample_save, 0, img.getHeight() * sizeof(int));

	for (i = 0; i < _nof_samples ; i++){
		rand_y = gen.generate();
		//save du numero de la ligne
		sample_save[cdf.inverseY(rand_y)]++;
	}

	for (y = 0; y < img.getHeight(); y++) {
		l = sample_save[y];
		for (i = 0; i < l; i++) {
			rand_x = gen.generate();
			x = cdf.inverseX(rand_x,y);
			v = img.getPixel(x, y, 0) + _image_sum_values / _nof_samples;
			img.setPixel(x, y, 0, std::max(std::min(v, 1.f), 0.f));
		}
	}
	free(sample_save);
}

void Pointillisme::twoStepParallelizedSampling(Image &img, CDF &cdf) const {
	//Generateur d'alea entre 0 et 1
	int *sample_save = (int*) malloc(img.getHeight() * sizeof(int));
	int y, i, l;
	memset(sample_save, 0, img.getHeight() * sizeof(int));
	#pragma omp parallel default(none) shared(cdf, img, sample_save, l) private(y, i)
	{
		Random gen(0., 1.);

		#pragma omp for reduction(+:sample_save[:img.getHeight()])
		for (i = 0; i < _nof_samples; i++) {
			float rand_y = gen.generate();
			sample_save[cdf.inverseY(rand_y)]++;
		}

		#pragma omp for schedule(static, img.getHeight()/12)
		for (y = 0; y < img.getHeight(); y++) {
			l = sample_save[y];
			for (i = 0; i < l; i++) {
				float rand_x = gen.generate();
				int x = cdf.inverseX(rand_x,y);
				float v = img.getPixel(x, y, 0) + _image_sum_values / _nof_samples;
				img.setPixel(x, y, 0, std::max(std::min(v, 1.f), 0.f));
			}
		}
	}

	free(sample_save);

}
