#ifndef IMAGEIO_H
#define IMAGEIO_H

#include <string>
#include "image.h"

class ImageIO
{
public:
    ImageIO();
    static void saveExtensionAuto(const Image& img, const std::string& filename);
    static void save(const Image & img, const std::string& filename);
    static bool load(Image & img, const std::string& filename);

protected:
    static void savePGM(const Image & img, const std::string& filename);
    static void savePPM(const Image & img, const std::string& filename);
    static bool loadPGM(Image & img, const std::string& filename);
    static bool loadPPM(Image & img, const std::string& filename);

private:
};

#endif // IMAGEIO_H
