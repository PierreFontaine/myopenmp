#include "image.h"

#include <cassert>
#include <iostream>

Image::Image()
{
}

Image::Image(const Image &img)
    :_width(img._width), _height(img._height), _layers(img._layers)
{
}

void Image::resize(int w, int h, int c)
{
    _width = w;
    _height = h;

    _layers.resize(c);
    for(int i=0; i<c; i++)
        _layers[i].resize(w,h);

}

/**
 * @brief Récupérer le pixel à la pos [i,j]
 * @param i
 * @param j
 * @param c choix RVB-Alpha
 * @return
 */
float Image::getPixel(int i, int j, int c) const
{
    assert(c<(int)_layers.size());
    return _layers[c].getPixel(i,j);
}

void Image::setPixel(int i, int j, int c, float value)
{
    assert(c<(int)_layers.size());
    _layers[c].setPixel(i,j,value);
}

void Image::debug()
{
      for(unsigned int i=0; i<_layers.size(); i++)
          _layers[i].debug();
}

Image::ImageType Image::getType() const
{
    return type;
}

void Image::setType(Image::ImageType t)
{
    type = t;
}
