#ifndef UTILS_H
#define UTILS_H

#endif // UTILS_H

#include <omp.h>
#include <cstdlib>

double time();
double elapsed(const double start, const double end);
