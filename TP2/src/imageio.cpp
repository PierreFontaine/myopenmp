#include "imageio.h"

#include <fstream>
#include <iostream>
#include <cassert>

ImageIO::ImageIO()
{
}

void ImageIO::saveExtensionAuto(const Image &img, const std::string& filename)
{
    assert(img.getNofLayer() == 1 || img.getNofLayer() == 3);
    if(img.getNofLayer() == 1)
        save(img, filename+".pgm");
    else if(img.getNofLayer() == 3)
        save(img, filename+".ppm");
}

void ImageIO::save(const Image &img, const std::string& filename)
{
    std::size_t found = filename.find_last_of(".");
    if((int)found == -1) {
        std::cerr << "Error : " << filename << " is not a file" << std::endl;
        return;
    }

    std::string extension = filename.substr(found);
    if(extension == ".pgm")
        savePGM(img, filename);
    else if(extension == ".ppm")
        savePPM(img, filename);
    else
    {
        std::cerr << "Error : " << filename << " is not compatible" << std::endl;
        return;
    }
}

bool ImageIO::load(Image &img, const std::string& filename)
{
    std::size_t found = filename.find_last_of(".");
    if((int)found == -1) {
        std::cerr << "Error : " << filename << " is not a file" << std::endl;
        return false;
    }

    std::string extension = filename.substr(found);
    if(extension == ".pgm")
        return loadPGM(img, filename);
    else if(extension == ".ppm")
        return loadPPM(img, filename);
    else
    {
        std::cerr << "Error : " << filename << " is not compatible" << std::endl;
        return false;
    }
}

void ImageIO::savePGM(const Image &img, const std::string& filename)
{
    std::ofstream fs;
    fs.open(filename.c_str());
    if(!fs.is_open()) {
        std::cerr << "Error : fail to open the file " << filename << std::endl;
        return;
    }
    fs << "P2" << std::endl;
    fs << img.getWidth() << " " << img.getHeight() << std::endl;
    fs << "255" << std::endl;
    for(int j=0; j<img.getHeight(); j++) {
        for(int i=0; i<img.getWidth(); i++) {
            fs << (int)(img.getPixel(i,j,0) * 255) << " ";
        }
        fs << std::endl;
    }

    fs.close();
}

void ImageIO::savePPM(const Image &img, const std::string& filename)
{
    std::ofstream fs;
    fs.open(filename.c_str());
    if(!fs.is_open()) {
        std::cerr << "Error : fail to open the file " << filename << std::endl;
        return;
    }

    assert(img.getNofLayer() == 3);

    fs << "P3" << std::endl;
    fs << img.getWidth() << " " << img.getHeight() << std::endl;
    fs << "255" << std::endl;
    for(int j=0; j<img.getHeight(); j++) {
        for(int i=0; i<img.getWidth(); i++) {
            for(int c=0 ; c<3 ; ++c)
                fs << (int)(img.getPixel(i,j,c) * 255) << " ";
        }
        fs << std::endl;
    }

    fs.close();
}

bool ImageIO::loadPGM(Image &img, const std::string& filename)
{
    std::ifstream fs;
    fs.open(filename.c_str());
    if(!fs.is_open()) {
        std::cerr << "Error : fail to open the file " << filename << std::endl;
        return false;
    }

    std::string magicNumber;
    fs >> magicNumber;
    if(magicNumber.compare("P2") != 0) {
        std::cerr << "Error : Format unknown " << magicNumber << std::endl;
        return false;
    }

    int width, height, maxVal;
    fs >> width;
    fs >> height;
    fs >> maxVal;

    img.resize(width,height,1);

    for(int j=0; j<height; j++) {
        for(int i=0; i<width; i++) {
            int val;
            fs >> val;
            img.setPixel(i,j,0,val/(float)maxVal);
        }
    }

    fs.close();
    img.setType(Image::GRAY_SCALE);

    return true;
}

bool ImageIO::loadPPM(Image &img, const std::string &filename)
{
    std::ifstream fs;
    fs.open(filename.c_str());
    if(!fs.is_open()) {
        std::cerr << "Error : fail to open the file " << filename << std::endl;
        return false;
    }

    std::string magicNumber;
    fs >> magicNumber;
    if(magicNumber.compare("P3") != 0) {
        std::cerr << "Error : Format unknown " << magicNumber << std::endl;
        return false;
    }

    int width, height, maxVal;
    fs >> width;
    fs >> height;
    fs >> maxVal;

    img.resize(width,height,3);

    for(int j=0; j<height; j++) {
        for(int i=0; i<width; i++)
            for(int c=0 ; c<3 ; ++c)
            {
                int val;
                fs >> val;
                img.setPixel(i,j,c,val/(float)maxVal);
            }
    }

    fs.close();
    img.setType(Image::RGB);

    return true;
}
