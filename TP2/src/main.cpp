#include <iostream>

#include "utils.h"
#include "image.h"
#include "imageio.h"
#include "pointillisme.h"
#include "histogramequalization.h"

void computePointillisme()
{
    double start, end;

    Image input;
    Pointillisme pointillisme;

    std::cout << "Open image... " << std::flush;
    start = time();
    ImageIO::load(input, "./data/starwars1.pgm");
    std::cout << elapsed(start, end=time()) << "s" << std::endl;

    start = time();
    pointillisme.setNofSamples( input.getWidth() * input.getHeight() * input.getNofLayer() * 5);
    pointillisme.process(input);
    std::cout << "Pointillisme processing... " << elapsed(start, end=time()) << "s" << std::endl;

    std::cout << "Save image... " << std::flush;
    start = time();
    ImageIO::save(input, "./data/pointillisme.pgm");
    std::cout << elapsed(start, end=time()) << "s" << std::endl;
}

void computeHistogramEqualization()
{
    bool useManualBoundary = false;

    Image input;
    double start, end;

    ImageIO::load(input, "./data/low_contrast.pgm");
    float min, max;
    if(useManualBoundary)
    {
        min = 0.380392;
        max = 0.607843;
    }
    else
    {
        start = time();
        minMaxSearchSequential(input,min,max);
        std::cout << "[HistogramEqu] Compute min / max naive version... " << elapsed(start, end=time()) << "s" << std::endl;
        std::cout << "[HistogramEqu] # min / max ==> " << min << " / " << max << std::endl;
        std::cout << std::endl;

        start = time();
        minMaxSearchReduction(input,min,max);
        std::cout << "[HistogramEqu] Compute min / max reduction1 version... " << elapsed(start, end=time()) << "s" << std::endl;
        std::cout << "[HistogramEqu] # min / max ==> " << min << " / " << max << std::endl;
        std::cout << std::endl;
    }

    start = time();
    Image img1 = input;
    equalizeSequential(img1,min,max);
    std::cout << "[HistogramEqu] Process naive... " << elapsed(start, end=time()) << "s" << std::endl;


    start = time();
    Image img2 = input;
    equalizeParallelized(img2,min,max);
    std::cout << "[HistogramEqu] Process parallel... " << elapsed(start, end=time()) << "s" << std::endl;

    ImageIO::save(img1, "./data/equalization_1.pgm");
    ImageIO::save(img2, "./data/equalization_2.pgm");
}

int main()
{

    computeHistogramEqualization();
    computePointillisme();
    return 0;
}
