#ifndef HISTOGRAMEQUALIZATION_H
#define HISTOGRAMEQUALIZATION_H

#include "image.h"

void minMaxSearchSequential(const Image& img, float & min,  float & max);
void equalizeSequential(Image &img, float min, float max);

void minMaxSearchReduction(const Image& img, float & min,  float & max);
void equalizeParallelized(Image &img, float min, float max);

#endif // HISTOGRAMEQUALIZATION_H
