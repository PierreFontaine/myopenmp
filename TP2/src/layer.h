#ifndef LAYER_H
#define LAYER_H

#include <vector>

class Layer
{
public:
    Layer();
    Layer(const Layer& l);
    ~Layer() {}

    void resize(int w, int h);

    float getPixel(int i, int j) const;
    float& getPixelRef(int i, int j)
    {
        int index = j * _width + i;
        return _pixels[index];
    }
    void setPixel(int i, int j, float value);

    void debug();

private:
    int _width, _height;
    std::vector<float> _pixels;
};

#endif // LAYER_H
