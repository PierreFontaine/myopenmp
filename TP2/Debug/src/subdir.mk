################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/cdf.cpp \
../src/histogramequalization.cpp \
../src/image.cpp \
../src/imageio.cpp \
../src/layer.cpp \
../src/main.cpp \
../src/pointillisme.cpp \
../src/utils.cpp 

OBJS += \
./src/cdf.o \
./src/histogramequalization.o \
./src/image.o \
./src/imageio.o \
./src/layer.o \
./src/main.o \
./src/pointillisme.o \
./src/utils.o 

CPP_DEPS += \
./src/cdf.d \
./src/histogramequalization.d \
./src/image.d \
./src/imageio.d \
./src/layer.d \
./src/main.d \
./src/pointillisme.d \
./src/utils.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -fopenmp -std=c++11 -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


