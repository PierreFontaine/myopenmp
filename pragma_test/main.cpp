#include <iostream>
#include <omp.h>

using namespace std;

int main (int argc, char const *argv[]) {
	int i;

	cout << "The boys are back in code !" << endl;

	cout << "[chorus]" << endl;
	#pragma omp for private(i)
	for (i = 0; i < 10; i++) {
		cout << "The boys are back in cooooode !" << endl;
	}

	return 0;
}
