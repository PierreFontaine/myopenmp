# Compte rendu de TP2

## Introduction

Dans ce TP nous allons étudier un programme destiné à faire du traitement d'image. Nous pouvons considérer qu'une image est une matrice de points, où chacun de ces points peut être représenté par un vecteur afin de définir les couleurs.

Certains traitements se font sur ces images en analysant les pixels de façon indépendantes, puis d'autres qui ont besoin de résultats de pixels adjacents avant d'effectuer un calcul.

## Note

La compilation des sources se fait via CMake. Une fois dans le dossier `src` vous pourrez effectuer les commandes suivantes :

```bash
cmake -S . -B <name_of_your_build_directory>
cd <name_of_your_build_directory>
make
```

## Amélioration de la dynamique

### Egalisation

Dans cette première partie nous allons développé un morceau de code permettant d'égaliser une image, puis implémenter sa version parallèle.

Le traitement à appliquer à chaque pixel est indépendant, en effet nous aurons uniquement besoin de la valeur courant du pixel ainsi que de deux autres variables indépendantes qui sont la valeur minimum et maximum de l'image.

La formule issue de l'énoncé est la suivante : 
$$
\nu' = \frac{\nu - \nu_{min}}{\nu_{max} - \nu_{min}}
$$

#### Séquentielle

Pour cette version $\nu_{min}$ ainsi que $\nu_{max}$ sont passés en tant qu'arguments à la fonction. Nous pouvons récupérer la hauteur ainsi que la largeur via les getters de l'objet `Image`.

Nous allons donc imbriquer 2 boucles `for` afin d'itérer sur chaque éléments de la matrice de façon classique.

```c
void equalizeSequential(Image &img, float min, float max) {
    int h = img.getHeight();
    int w = img.getWidth();

    for (int i = 0; i < h; i++) {
    	for (int j = 0; j < w; j++) {
			float pixel = img.getPixel(j,i,0);
			float newPixel = (pixel - min) / (max - min);
			img.setPixel(j,i,0,newPixel);
    	}
    }
}
```

Suivant cette implémentation nous obtenons ce temps d'execution : `[HistogramEqu] Process naive... 0.0200741s`

###  Parallèle

Pour implémenter la versions parallèle, nous n'allons pas changer la structure de notre code, nous allons placer aux bons endroits les bonnes directives.

Typiquement ici, nous avons de boucles imbriqués sans dépendances aux niveaux des variables d'itérations. Nous allons donc appliquer un `pragma omp for`. 

Nous partageons entre chaque threads les bornes max d'itérations, les arguments de la fonction `min` et `max`, ainsi que l'instance d'objet image. Notons ici que `img` est passé par référence.

```c
void equalizeParallelized(Image &img, float min, float max) {
	int h = img.getHeight();
	int w = img.getWidth();
	int i,j; //Iterateur

	float pixel, newPixel;

	#pragma omp parallel default(none) shared(h,w, min, max, img) private(i,j, pixel, newPixel)
	{
		#pragma omp for collapse(2)
		for (i = 0; i < h; i++) {
			for (j = 0; j < w; j++) {
				pixel = img.getPixel(j,i,0);
				newPixel = (pixel - min) / (max - min);
				img.setPixel(j,i,0,newPixel);
			}
		}
	}
}
```

Ainsi, nous obtenons de bien meilleurs performances : `[HistogramEqu] Process parallel... 0.00649298s`

## Calcul du maximum et du minimum

Ces deux valeurs qui sont `maximum` et `minimum`, nous nous en sommes servie dans notre fonction étudiée précédemment. Cependant avant de s'en servir il faut les calculer et ces calculs se font eux aussi en parcourant l'image sur chaque pixels.

### Séquentielle

Ici le traitement est similaire à ce que nous avons pu faire avant, nous avons 2 boucles. Celles ci vont parcourir la matrice représentant l'image.

```c
void minMaxSearchSequential(const Image &img, float &min, float &max) {
    min = std::numeric_limits<float>::max();
    max = std::numeric_limits<float>::min();

    for(int j=0 ; j<img.getHeight() ; ++j)
        for(int i=0 ; i<img.getWidth() ; ++i) {
            float v = img.getPixel(i, j, 0);
            if( min > v ) min = v;
            if( max < v ) max = v;
        }
}
```

Sur cette version nous obtenons les performances suivantes : `[HistogramEqu] Compute min / max naive version... 0.00852143s`

### Parallèle

Dans la versions parallèle nous allons une fois de plus ajouter une directive `pragma omp for`. Dans la région parallèle nous allons définir les variables partagées suivantes : 

- image : `img`
- bornes d'itération: `h` et `w`
- variables de retour: `min` et `max`

Les variables privées seront les suivantes : 

- variables itérations : `i` et `j`
- valeur du pixel[i,h] : `v`

Ici nous avons un accès concurrentiel aux variables `min` et `max`. Une solution pourrait être d'établir une section critique avant l'accès à cette variable, cependant OpenMP nous fournit une directive qui se prête bien à ce genre de situation. Cette directive est la `reduction`, dans notre cas nous l'utiliserons avec l'option *min* et l'option *max*. L'idée derrière cette réduction est de dire le max de l'image sera le max de chaque max trouvé par les *Teams* de thread, respectivement pour le min.

```cpp
void minMaxSearchReduction(const Image &img, float &min, float &max) {

	min = std::numeric_limits<float>::max();
	max = std::numeric_limits<float>::min();
	int i,j;
	float v;
	int h = img.getHeight();
	int w = img.getWidth();

	#pragma omp parallel default(none) shared(img, h, w, min, max) private(v,i,j) reduction(min:min) reduction(max:max)
	{
		#pragma omp for collapse(2)
		for(j=0 ; j<h ; ++j) {
			for(i=0 ; i<w ; ++i){
				v = img.getPixel(i, j, 0);
				if( min > v )
					min = v;
				if( max < v )
					max = v;
			}
		}
	}
}
```

Avec cette implémentation nous obtenons les performances suivantes : `[HistogramEqu] Compute min / max reduction1 version... 0.00181735s`

Pour information, les performances obtenues avec sections critiques gérées via la directive `omp critical` sont  : `[HistogramEqu] Compute min / max reduction1 version... 0.0024317s` ce qui nous donne une accélération parallèle de $1.338​$.

## Pointillisme

### Sampling

#### Étude de la version séquentielle

Cette algorithme met en place un générateur aléatoire ainsi qu'une boucle `for` qui va itérer sur le nombre de sample prédéfinis.

Nous noterons ici que les valeurs `x` et `y` sont dérivées de `rand_x` ainsi que `rand_y` via les méthodes `cdf.inverse{Y,X}`. De ce fait nous pouvons avoir pour 2 itérations différentes des valeurs de `x` et `y` égales. Nous devrons donc prendre en compte cette spécificité pour la version parallèle afin d'éviter un accès concurrent.

Pour la version séquentielle implémentée nous pouvons l'effectuer en 14.0125s.

#### Implémentation de la version parallèle

Pour cette version parallèle nous allons faire attention à placer le générateur dans la région parallèle. Cela permettra que chaque thread aient un générateur instanciés et aient donc des *seeds* différents. Ainsi nous éviterons l'apparition de patterns.

Nous allons nous occuper d'une boucle `for`, de la même façon que les autres nous privatisons les variables d'itérations (`i`), et les variables internes indépendantes (`y,x,v,rand_x,rand_y`). Nous partageons l'image (`img`) ainsi que la fonction de répartition (`cdf`).

Comme nous avons pu l'évoquer précédemment, il est possible d’accéder aux mème coordonnées de l'image pour 2 itérations différentes. Nous allons donc utiliser la directive `pragma omp critical` afin d'expliciter que l'accès à l'instruction `img.setPixel()` est une ressource partagée.

```cpp
void Pointillisme::parallelizedSampling(Image &img, CDF &cdf) const {
   uint i;
   int x, y;
   float rand_x, rand_y, v;

   #pragma omp parallel default(none) shared(cdf, img) private(rand_x, rand_y, i, y, x, v)
   {
      Random gen(0., 1.);
      #pragma omp for
      for(i=0 ; i < _nof_samples ; ++i) {
         rand_x = gen.generate();
         rand_y = gen.generate();

         y = cdf.inverseY(rand_y);
         x = cdf.inverseX(rand_x,y);
         v = img.getPixel(x, y, 0) + _image_sum_values / _nof_samples;
         #pragma omp critical
         img.setPixel(x, y, 0, std::max(std::min(v, 1.f), 0.f));
      }
   }
}
```

Cette version parallèle s’exécute avec la performance suivante : `[Pointillisme] Generate samples naive parallelized version... 2.19395s`. Nous avons ici une accélération parallèle qui vaut $6.38​$.

#### TwoStepSampling

Dans cette version nous allons utiliser une table de hashage. Celle ci étant de la forme `Int -> Int` nous pouvons directement utiliser un tableau simple. Dans cette table nous allons associer le nombre de colonnes à calculer pour chaque lignes.

Une fois que nous avons notre map remplie, nous aurons donc une itération qui s'occupera de la ligne 1 et la n_ème itérations s'occupera de la ligne n.

```c++
void Pointillisme::twoStepSampling(Image &img, CDF &cdf) const {
   //Generateur d'alea entre 0 et 1
   Random gen(0., 1.);
   float rand_x, rand_y, v;
   int i, x, l, y;
   int *sample_save = (int*) malloc(img.getHeight() * sizeof(int));
   memset(sample_save, 0, img.getHeight() * sizeof(int));

   for (i = 0; i < _nof_samples ; i++){
      rand_y = gen.generate();
      //save du numero de la ligne
      sample_save[cdf.inverseY(rand_y)]++;
   }

   for (y = 0; y < img.getHeight(); y++) {
      l = sample_save[y];
      for (i = 0; i < l; i++) {
         rand_x = gen.generate();
         x = cdf.inverseX(rand_x,y);
         v = img.getPixel(x, y, 0) + _image_sum_values / _nof_samples;
         img.setPixel(x, y, 0, std::max(std::min(v, 1.f), 0.f));
      }
   }
   free(sample_save);
}
```

Pour les performances nous obtenons le temps suivant : `[Pointillisme] Generate samples two step sequential version... 10.0952s` , ce qui est même plus performant que notre première version séquentielle.

#### TwoStepParallelizedSampling

Avec cette implémentation nous n'avons plus d'accès concurrents (attention à ne pas paralléliser la boucle `for` interne).

Pour la première boucle `for` nous pouvons effectuer une réduction sur l'auto incrémentions.

Pour la seconde boucle `for` nous parallélisons que celle supérieur, ainsi 1 thread s'occupera d'une ligne.

```c++
void Pointillisme::twoStepParallelizedSampling(Image &img, CDF &cdf) const {
   //Generateur d'alea entre 0 et 1
   int *sample_save = (int*) malloc(img.getHeight() * sizeof(int));
   int y, i, l;
   memset(sample_save, 0, img.getHeight() * sizeof(int));
   #pragma omp parallel default(none) shared(cdf, img, sample_save, l) private(y, i)
   {
      Random gen(0., 1.);

      #pragma omp for reduction(+:sample_save[:img.getHeight()])
      for (i = 0; i < _nof_samples; i++) {
         float rand_y = gen.generate();
         sample_save[cdf.inverseY(rand_y)]++;
      }

      #pragma omp for
      for (y = 0; y < img.getHeight(); y++) {
         l = sample_save[y];
         for (i = 0; i < l; i++) {
            float rand_x = gen.generate();
            int x = cdf.inverseX(rand_x,y);
            float v = img.getPixel(x, y, 0) + _image_sum_values / _nof_samples;
            img.setPixel(x, y, 0, std::max(std::min(v, 1.f), 0.f));
         }
      }
   }
   free(sample_save);
}
```

Le résultat de performance est le suivant : `[Pointillisme] Generate samples two step parallelized version... 1.63184s`

Ce qui est 8.6 fois plus rapide que la version séquentielle naïve, et 1.34 fois plus rapide que la version parallèle à section critique.

## Conclusion

Dans ce TP il a était mis en avant que le parallélisme à un grand intérêt lorsque que nous faisons du traitement d'image. D'autre part nous avons vu qu'il était intéressant de trouver une alternative aux sections critiques, que ce soit en utilisant une directive spécifique d'OpenMP ou que ce soit en changeant l'algorithme, sur les deux exemples nous avons eu une accélération de 1.34 environ.