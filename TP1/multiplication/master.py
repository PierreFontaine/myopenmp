# !python3
# coding: utf-8

import os
import sys
import re
import numpy as np
import matplotlib.pyplot as plt


# Function that parse the res and find the time
def parse_wtime(out_cmd):
    float_res = re.findall(r"[+-]?\d+\.\d*e?[+-]?\d*", out_cmd)

    if (float_res):
        return float(float_res.pop())
    else:
        print "error : "
        print out_cmd

if __name__ == "__main__":

    sample = 10

    ax_l = []
    fig, ax = plt.subplots()

    res_cmd1 = []
    res_cmd2 = []
    res_cmd3 = []
    res_cmd4 = []
    res_cmd5 = []

    cmd1 = "./prog.out"
    cmd2 = "./progPar.out"
    cmd3 = "./progPar2.out"
    cmd4 = "./progPar3.out"
    cmd5 = "./progPar4.out"

    for _ in range(sample):
        res_cmd1.append(parse_wtime(os.popen(cmd1).read()))

    print("[-] ",cmd1, " sampled")

    for _ in range(sample):
        res_cmd2.append(parse_wtime(os.popen(cmd2).read()))

    print("[-] ",cmd2, " sampled")

    for _ in range(sample):
        res_cmd3.append(parse_wtime(os.popen(cmd3).read()))

    print("[-] ",cmd3, " sampled")

    #for _ in range(sample):
     #   res_cmd4.append(parse_wtime(os.popen(cmd4).read()))

    # print("[-] ",cmd4, " sampled")

    for _ in range(sample):
        res_cmd5.append(parse_wtime(os.popen(cmd5).read()))
        
    print("[-] ",cmd5, " sampled")

    ax_l.append(ax.bar("mult", np.mean(res_cmd1), color=(0.2, 0.4, 0.6, 0.6), label="mult"))
    ax_l.append(ax.bar("mult"+'_p', np.mean(res_cmd2), color=(0.2, 0.9, 0.6, 0.6), label="mult_par"))
    ax_l.append(ax.bar("mult"+'_p2', np.mean(res_cmd3), color=(0.2, 0.6, 0.6, 0.6), label="mult_par_2"))
    #ax_l.append(ax.bar("mult"+'_p3', np.mean(res_cmd4), color=(0.4, 0.6, 0.6, 0.6), label="mult_par_3"))
    ax_l.append(ax.bar("mult"+'_p4', np.mean(res_cmd5), color=(0.8, 0.6, 0.6, 0.6), label="mult_par_4"))

    ax.set_title('comparaison multiplication')
    ax.set_ylabel('temps execution moyen')
    plt.legend(loc='best')
    plt.show()
    