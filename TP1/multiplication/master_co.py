# !python3
# coding: utf-8

import os
import sys
import re
import numpy as np
import matplotlib.pyplot as plt


# Function that parse the res and find the time
def parse_wtime(out_cmd):
    float_res = re.findall(r"[+-]?\d+\.\d*e?[+-]?\d*", out_cmd)

    if (float_res):
        return float(float_res.pop())
    else:
        print "error : "
        print out_cmd

if __name__ == "__main__":

    sample = 10

    ax_l = []
    fig, ax = plt.subplots()

    res_cmd4 = []
    res_cmd5 = []

    cmd4 = "./progPar5.out"
    cmd5 = "./progPar4.out"

    for _ in range(sample):
        res_cmd4.append(parse_wtime(os.popen(cmd4).read()))

    print("[-] ",cmd4, " sampled")

    for _ in range(sample):
        res_cmd5.append(parse_wtime(os.popen(cmd5).read()))
        
    print("[-] ",cmd5, " sampled")

    ax_l.append(ax.bar("mult "+ cmd4, np.mean(res_cmd4), color=(0.4, 0.6, 0.6, 0.6), label="mult coll 3"))
    ax_l.append(ax.bar("mult "+ cmd5, np.mean(res_cmd5), color=(0.8, 0.6, 0.6, 0.6), label="mult coll 2"))

    ax.set_title('comparaison multiplication sur collapse')
    ax.set_ylabel('temps execution moyen')
    plt.legend(loc='best')
    plt.show()
    