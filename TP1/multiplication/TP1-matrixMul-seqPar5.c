/******************************************************************************
* FILE: omp_mm.c
* DESCRIPTION:
*   OpenMp Example - Matrix Multiply - C Version
*   Demonstrates a matrix multiply using OpenMP. Threads share row iterations
*   according to a predefined chunk size.
* AUTHOR: Blaise Barney
* LAST REVISED: 06/28/05
******************************************************************************/
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>

#define NRA 1000                 /* number of rows in matrix A */
#define NCA 1000                 /* number of columns in matrix A */
#define NCB 1000                  /* number of columns in matrix B */

int main (int argc, char *argv[]) {
	int	i, j, k;

	double **a = malloc(NRA * sizeof(double)); /* matrix A to be multiplied */
	double **b = malloc(NCA * sizeof(double)); /* matrix B to be multiplied */
	double **c = malloc(NRA * sizeof(double)); /* result matrix C */

	for (int varA = 0; varA < NRA; ++varA) {
		a[varA] = malloc(NCA * sizeof(double));
	}

	for (int varB = 0; varB < NCA; ++varB) {
		b[varB] = malloc(NCB * sizeof(double));
	}

	for (int varC = 0; varC < NRA; ++varC) {
		c[varC] = malloc(NCB * sizeof(double));
	}

printf("Initializing matrices...\n");
    double start = omp_get_wtime();
/*** Spawn a parallel region explicitly scoping all variables ***/
#pragma omp parallel shared(a,b,c) private(i,j,k)
  {
  /*** Initialize matrices ***/
  #pragma omp for nowait collapse(1)
  for (i=0; i<NRA; i++)
    for (j=0; j<NCA; j++)
      a[i][j]= i+j;
  #pragma omp for nowait collapse(1)
  for (i=0; i<NCA; i++)
    for (j=0; j<NCB; j++)
      b[i][j]= i*j;
  #pragma omp for nowait collapse(1)
  for (i=0; i<NRA; i++)
    for (j=0; j<NCB; j++)
      c[i][j]= 0;

  #pragma omp barrier

  #pragma omp for collapse(3)
  for (i=0; i<NRA; i++){
    for(j=0; j<NCB; j++)
      for (k=0; k<NCA; k++)
        c[i][j] += a[i][k] * b[k][j];
    }
  }   /*** End of parallel region ***/

  double end = omp_get_wtime();

  	/*** Print results ***/
  	/**
  	printf("******************************************************\n");
  	printf("Result Matrix:\n");
  	for (i = 0; i < NRA; i++) {
  		for (j = 0; j < NCB; j++)
  			printf("%6.2f   ", c[i][j]);
  		printf("\n");
  	}
  	 **/
  	printf("******************************************************\n");
  	printf("Time = %.16g sec\n", end - start);
  	printf("Done.\n");

}
