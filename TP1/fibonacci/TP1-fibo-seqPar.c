#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <omp.h>

long fibo(int n, int cut) {
	long F_1, F_2;
	if (n <= 1)
		return n;
	else {
		if (n < cut) {
			return fibo(n-1, cut) + fibo(n-2, cut);
		} else {
			#pragma omp task shared(F_1)
			{
					F_1 = fibo(n-1, cut);
			}
			#pragma omp task shared(F_2)
			{
					F_2 = fibo(n-2, cut);
			}
			#pragma omp taskwait
			{
				return (F_1 + F_2);
			}
		}

	}


}

int main(int argc, char *argv[]) {
	/*  Arguments  argv[0]:            */
	/*  0   -> (executable)            */
	/*  1   -> Taille du tableau       */

	int fiboSize;
	long nb;

	/* Validation du nombre d'arguments */
	if (argc != 3) {
		fprintf( stdout, "usage:  %s  number\n", argv[0]);
		exit(-1);
	}

	/* Récupération de la taille des tableaux */

	fiboSize = (int) atof(argv[1]);

	printf("Starting fibo...\n");
	double start = omp_get_wtime();

#pragma omp parallel default(none) shared(fiboSize, argv)
{
	#pragma omp single
	{
		nb = fibo(fiboSize, (int) atof(argv[2]));
	}
}

	double end = omp_get_wtime();
	printf("End fibo\n nb = %ld\n",nb);
	printf("Time seq = %.16g for fibo = %d\n", end - start, fiboSize);

	printf("\nProgram successfully terminated.\n\n");
	return (0);
}
