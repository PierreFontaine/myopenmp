# !python3
# coding: utf-8

import os
import sys
import re
import numpy as np
import matplotlib.pyplot as plt


# Function that parse the res and find the time
def parse_wtime(out_cmd):
    float_res = re.findall(r"[+-]?\d+\.\d*e?[+-]?\d*", out_cmd)

    if (float_res):
        return float(float_res[0])
    else:
        print "error : "
        print out_cmd

if __name__ == "__main__":

    sample = 1
    argv = [10, 20, 25, 30, 35, 40]
    ax_l = []
    fig, ax = plt.subplots()

    for i, arg in enumerate(argv):
        res_cmd1 = []
        res_cmd2 = []

        cmd1 = "./prog.out " + str(arg)
        cmd2 = "./progPar.out " + str(arg)

        for _ in range(sample):
            res_cmd1.append(parse_wtime(os.popen(cmd1).read()))

        for _ in range(sample):
            res_cmd2.append(parse_wtime(os.popen(cmd2).read()))

        if i == 0:
            ax_l.append(ax.bar(str(arg), np.mean(res_cmd1), label='non par', color=(0.2, 0.4, 0.6, 0.6)))
            ax_l.append(ax.bar(str(arg)+'_p', np.mean(res_cmd2), label='para', color=(0.2, 0.9, 0.6, 0.6)))
        else:
            ax_l.append(ax.bar(str(arg), np.mean(res_cmd1), color=(0.2, 0.4, 0.6, 0.6)))
            ax_l.append(ax.bar(str(arg)+'_p', np.mean(res_cmd2), color=(0.2, 0.9, 0.6, 0.6)))

    ax.set_title('comparaison boucle taille')
    ax.set_ylabel('temps execution moyen')
    plt.legend(loc='best')
    plt.show()
    