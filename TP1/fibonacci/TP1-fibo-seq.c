/*  Fichier arrayMaj.c            */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <omp.h>

long fibo(int n) {

	if (n <= 1)
		return n;
	else
		return fibo(n - 1) + fibo(n - 2);
}

int main(int argc, char *argv[]) {
	/*  Arguments  argv[0]:            */
	/*  0   -> (executable)            */
	/*  1   -> Taille du tableau       */

	int fiboSize;
	long nb;

	/* Validation du nombre d'arguments */
	if (argc != 2) {
		fprintf( stdout, "usage:  %s  number\n", argv[0]);
		exit(-1);
	}

	/* Récupération de la taille des tableaux */
	fiboSize = (int) atof(argv[1]);

	printf("Starting fibo...\n");
	double start = omp_get_wtime();
	nb = fibo(fiboSize);
	double end = omp_get_wtime();
	printf("End fibo\n nb = %ld\n",nb);
	printf("Time seq = %.16g for fibo = %d\n", end - start, fiboSize);

	printf("\nProgram successfully terminated.\n\n");
	return (0);
}
