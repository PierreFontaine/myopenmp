# Compte rendu de TP1

## Introduction

Jusqu'ici la plus part de nos implémentation s’exécutaient sur un unique thread. Aujourd'hui nos machines disposent de plusieurs cœurs logiques. Si ces derniers sont mis à contribution de façon à ce qu'ils se partagent les taches, il sera possible de faire de sérieux gains de temps.

Pour ce TP les sources (codes modifiés + scripts Python) seront disponibles ici <https://gitlab.com/PierreFontaine/myopenmp>

## Hello World

### Question 1

Ici nous déclarons une région parallèle avec une team de threads qui s'élevera au nombre de coeurs logique que nous possèdons sur notre ordinateur.
Pour ma part je dispose de 12 coeurs logiques.

Chacun de ces threads possèderont leurs propre copie de la variable `numThreads` et de la variable `tid`.


### Question 2

Nous pouvons constater que notre sortie n'est pas déterministe. En effet 2 executions successives mènent à 2 sorties différentes (sauf coup de chance).

La seule composante déterministe que nous avons ici est le fait que la sortie `Number of threads ...` sera précédé par la sortie du thread 0.

```bash
Hello World from thread number 1
Hello World from thread number 0
Number of threads is 12
Hello World from thread number 9
Hello World from thread number 10
Hello World from thread number 8
Hello World from thread number 2
[...]
```

## Utilisation de directives OpenMP simples

### OpenMP, etude des performances

Nous allons nous pencher sur l'implémentation de la boucle effectuant le calcul demandant le plus de ressources.

```c
for (i = 0; i < arraySize; i++) {
	y[i] = sin(exp(cos(-exp(sin(x[i])))));
}
```

Nous allons ici pouvoir ajouter une directive permettant la paralellisation de cette boucle.

```c
#pragma omp parallel default(none) shared(arraySize,x,y) {
	#pragma omp for private(i)
	for (i = 0; i < arraySize; i++) {
		y[i] = sin(exp(cos(-exp(sin(x[i])))));
	}
}
```

Ici les variables `arraySize`, `x` ainsi que `y` ne sont pas modifiées au cours de l'execution de cette région, nous pouvons donc les partagées entre chaque threads.
En revanche la variable `i` elle sera privée.

Pour une mesure des performances nous allons développer un script qui va automatiser l'appel des fonctions et récupérer les temps d’exécution pour différents valeurs passées.

![](./array_maj/stats.png)

Nous pouvons constater qu'à partir de 1e5 itérations, la parallélisation devient rentable.

### OpenMP, multiple variables

Ici nous imbriquons une boucle dans celle existante :

```c
int cnt = 0;
...
	#pragma omp parallel default(none) shared(arraySize,x,y,cnt)
	{
		#pragma omp for private(i, ind, indPrim)
		for (i = 0; i < arraySize; i++) {
			y[i] = sin(exp(cos(-exp(sin(x[i])))));
			for (ind = 0; ind < 16; ind++) {
				cnt++;
				indPrim = ((double) ind) / 16;
				y[i] = sin(exp(cos(-exp(sin(x[i]))))) + cos(indPrim);
			}
		}
	}
```

Nous mettons `cnt` dans notre boucle la plus interne et la mettons en `shared`, ainsi cette variable est global et est vue par chaque processus.

Le problème, est que pour différentes exécutions, nous aurons différentes valeurs pour `cnt`  à la fin (i.e. 390, 700 ...).

Nous pourrions imaginer que faire passer `cnt` en variable privée puisse régler le problème, cependant non.

Ce qui pourra résoudre notre problème, sera de laisser `cnt` dans la liste des variables partagée mais d'ajouter la clause `omp atomic` avant l'instruction qui se charge d'incrémenter le compteur.

```c
#pragma omp parallel default(none) shared(arraySize,x,y, cnt)
{
    #pragma omp for private(i, ind, indPrim)
    for (i = 0; i < arraySize; i++) {
        y[i] = sin(exp(cos(-exp(sin(x[i])))));
        for (ind = 0; ind < 16; ind++) {
            #pragma omp atomic
            cnt++;
            indPrim = ((double) ind) / 16;
            y[i] = sin(exp(cos(-exp(sin(x[i]))))) + cos(indPrim);
        }
    }
}
```

Nous aurons ainsi un résultat déterministe, ici notre compteur renverra toujours la valeur 1600.

## OpenMP et fonctions complexes

### Multiplication de matrices

#### Intro

L'objectif ici est de tenter d'optimiser le déroulement d'une multiplication matricielle. De façon naïve il semblerait que le parallélisme soit le mieux approprié pour ce genre d'exercice.

Cependant nous allons vite nous rendre compte qu'il vaut mieux rester sur un déroulement séquentiel pour ce genre d'opération.

#### Modification du code

Analysons le code, nous pouvons remarquer que nos 3 premières boucles `for` sont indépendantes. Nous allons pouvoir appliquer la clause `nowait`.

Pour ce qui est des variables nous passons les vecteurs en partagé, puis les variables d'itération en privé.

Du fait que chacune de ces boucles sont *nested* nous allons appliquer la clause `collapse(2)` ayant pour paramètre le nombre de boucles imbriqués, ce qui va permettre au compilateur d'optimiser le code.

Maintenant la partie qui demande le plus d'attention est la 4ème boucle, en effet celle ci dépend des résultats précédents, nous allons commencer par appliquer une synchronisation des threads grâce à la clause `barrier`. Maintenant nous avons 3 niveaux d’imbrication indépendantes (par rapport aux variables d'itérations) ce qui nous amène à insérer la clause `collapse(3)`. Pour finir ici nous avons une subtilité, en effet nous avons une auto affectation récurrente, ce qui va nous permettre d'utiliser une réduction via `reduction(+:c)`.



```c
#pragma omp parallel default(none) shared(a,b,c) private(i,j,k)
{
	#pragma omp for nowait collapse(2)
	for (i = 0; i < NRA; i++) {
		for (j = 0; j < NCA; j++)
			a[i][j] = i + j;
	}

	#pragma omp for nowait collapse(2)
	for (i = 0; i < NCA; i++) {
		for (j = 0; j < NCB; j++) {
			b[i][j] = i * j;
		}
	}

	#pragma omp for nowait collapse(2)
	for (i = 0; i < NRA; i++) {
		for (j = 0; j < NCB; j++) {
			c[i][j] = 0;
		}
	}

	#pragma omp barrier
	#pragma omp for reduction(+:c) collapse(3)
	for (i = 0; i < NRA; i++) {
		for (j = 0; j < NCB; j++) {
			for (k = 0; k < NCA; k++) {
				c[i][j] += a[i][k] * b[k][j];
			}
		}
	}
}
```

Une fois le code modifié et compilé, vient le temps des performances. Nous reprenons notre code Python et faisons en sorte qu'il sorte les statistiques suivante, la moyenne de 100 exécutions sur une multiplication séquentielle, puis la moyenne de 100 exécutions sur une multiplication parallélisée. 

Contre toute attenté la solution séquentielle est plus rapide que celle parallélisée, comme nous pouvons le remarqué dans l'histogramme ci dessous.

![](./multiplication/Figure_1.png)

Il est légitime de se demander si il est bien nécéssaire de paralléliser les 3 premières boucles qui demandent d'être synchronisées avant la toute dernière étape.

Nous allons donc tenter d'optimiser seulement la dernière boucle.

```c
#pragma omp parallel default(none) shared(a,b,c) private(i,j,k)
{
	#pragma omp for reduction(+:c) collapse(3)
	for (i = 0; i < NRA; i++) {
		for (j = 0; j < NCB; j++) {
			for (k = 0; k < NCA; k++) {
				c[i][j] += a[i][k] * b[k][j];
			}
		}
	}
}
```

Les resultats de performances ne sont pas flagrants.

![](./multiplication/Figure_2.png)

Pour ce qui est du fait de ne paralélliser que les 3 premières boucles, voici les résultats. Ils sont très proches de ceux que l'on a obtenues à l'étape précédente.

![](./multiplication/Figure_3.png)

Le fait est qu'ici nous sommes sur des matrices de petites tailles. Pour pouvoir augmenter la taille de nos matrices et effectuer d'autres tests nous allons passer par des mallocs. Les mallocs vont nous permettre d'augmenter la taille de nos matrices en passant par la heap, en effet jusqu'ici nos matrices étaient déclarées de façon statique autrement dit sur la stack, et cette dernière possède une taille limitée.

![](./multiplication/Figure_5.png)

Cette fois nous pouvons observer que nos résultats sont bien meilleurs, nous avons une accélération égale à 7.

Maintenant nous allons nous focaliser sur la gestion statique et de la taille des chuncks que nous allons allouer.

```c
#pragma omp for collapse(3) schedule(static, 83)
  for (i=0; i<NRA; i++){
    for(j=0; j<NCB; j++)
      for (k=0; k<NCA; k++)
        c[i][j] += a[i][k] * b[k][j];
    }
  }   /*** End of parallel region ***/
```

Avec un ordonnancement statique nous sommes à $$0.8758991849899758​$$ s

```bash
Initializing matrices...
******************************************************
Time = 0.8758991849899758 sec
Done.

```

Alors que sans ordonnancement nous étions à $$1.115129903017078 $$s.

```c
#pragma omp for collapse(3)
  for (i=0; i<NRA; i++){
    for(j=0; j<NCB; j++)
      for (k=0; k<NCA; k++)
        c[i][j] += a[i][k] * b[k][j];
    }
  }   /*** End of parallel region ***/
```

```bash
Initializing matrices...
******************************************************
Time = 1.115129903017078 sec
Done.
```

Nous avons une accélération de $$1.115129903017078 / 0.8758991849899758 = 1.2731258598326474$$ en utilisant un l'ordonnancement statique est divisant le nombre de bloques par le nombre de threads présents sur la machine.

### Suite de Fibonacci

Ici notre suite de fibonacci est implémentée sous forme d'une fonction récursive terminale.

Pour bien voir comment se comporte une tel fonction, et comment sont gérées ces appels récursifs nous allons poser ici une trace lisp via DrRacket.

```lisp
> (require racket/trace)
> (define (fibo n)
    (cond
      ((= n 0) 0)
      ((= n 1) 1)
      (true (+ (fibo (- n 1)) (fibo (- n 2))))
      )
    )

>(fibo 8)
> (fibo 7)
> >(fibo 6)
> > (fibo 5)
> > >(fibo 4)
> > > (fibo 3)
> > > >(fibo 2)
> > > > (fibo 1)
< < < < 1
...
> > > (fibo 2)
> > > >(fibo 1)
< < < <1
> > > >(fibo 0)
< < < <0
...
<21
21
```



Nous allons devoir ici utiliser la notion de `task`, ainsi nous pourrons paralléliser les branches d'appels récursif.



Par exemple, toujours en prenant notre trace Lips, pour `fibo(8)` l'idée pour optimiser cela serait de paralléliser :

```lisp
>(fibo 8)

[thread 1]				[Thread 2]
> (fibo 7)				> (fibo 6)
> >(fibo 6)				> >(fibo 5)
> > (fibo 5)			> > (fibo 4)
> > >(fibo 4)			> > >(fibo 3)
> > > (fibo 3)			> > > (fibo 2)
> > > >(fibo 2)			> > > >(fibo 1)
> > > > (fibo 1)		
....					...
```

L'implémentation suivante est basée sur un cours que l'on peut trouver à l'adresse suivante : https://cse.iitkgp.ac.in/~debdeep/courses_iitkgp/PAlgo/Autumn16-17/slides/recursive_notes.pdf

```c
long fibo(int n) {
	long F_1, F_2;
	if (n <= 1)
		return n;
	else {
        #pragma omp task shared(F_1)
        {
            F_1 = fibo(n-1);
        }
        #pragma omp task shared(F_2)
        {
            F_2 = fibo(n-2);
        }
        #pragma omp taskwait
        {
            return (F_1 + F_2);
        }
    }
}

int main (int argc, char *argv[]) {
    ...
    #pragma omp parallel
    {
        #pragma omp single
        {
            nb = fibo(fiboSize);
        }
    }
}
```

Maintenant pour ce qui est question de performances, il semble encore une fois que le parallélisme ne fait qu'empirer les choses.

![](./fibonacci/Figure_1.png)

D'où peut provenir notre perte de temps ? Ce qu'il faut voir ici c'est que plus notre nombre passé en paramètre est grand plus nous allons empiler les appels et ceci de façon polynomial.

Notre fonction va exécuter (empilement/dépilement) un grand nombre de fois (dépendant de l'argument), de petite pile d'appels. 

Ce qui serait donc intéressant d'essayer est donc définir une limite en admettant qu'a partir un n fixé une récursion est plus rapide séquentiellement.

Ceci nous amène au programme suivant :

```c
long fibo(int n) {
	long F_1, F_2;
	if (n <= 1)
		return n;
	else {
		if (n < 30) {
			return fibo(n-1) + fibo(n-2);
		} else {
			#pragma omp task shared(F_1)
			{
					F_1 = fibo(n-1);
			}
			#pragma omp task shared(F_2)
			{
					F_2 = fibo(n-2);
			}
			#pragma omp taskwait
			{
				return (F_1 + F_2);
			}
		}
	}
}
```

![](./fibonacci/Figure_2.png)

Cette fois nous observons un gain non négligeable en performance !

![](./fibonacci/Figure_3.png)

### QuickSort

L'algorithme du `QuickSort` est une algorithme de tri récursif d'une compléxité $$nlog(n)$$.

#### Séquentiel

Au lancement de la version séquentiel de l'algorithme, pour un tableau de $$10000000$$ dont chacune des cases à été initialisé avec une variable aléatoire, le temps d’exécution sera autour des 12 secondes sur la machine.

```bash
[pfontaine@precision cmake-build-debug]$ ./quicksort_seq 
Starting quick sort...
End QuickSort
Time seq = 12.10129112901632 for quicksort on array size = 10000009
```

#### Parallèle v1

Pour cette version parallèle, nous allons nous inspirer de ce que nous avons pu faire avec l'algorithme de Fibonacci. L'idée sera donc d'utiliser les primitives `Tasks`.

Dans notre main, nous déclarerons une zone parallèle afin de définir une `team` de threads (qui seront à même d’exécuter les futurs tâches dans le pool de tâches)

```c
...
#pragma omp parallel default(none) shared(a,n)
{
    #pragma omp single nowait
    {
        qsort_para(0, n - 1);
    }
}
```



Maintenant nous allons nous pencher sur la fonction récursive appelée dans notre zone parallèle. Celle ci sépare le tableau (variable global) en 2 partie et se rappelle deux fois pour traiter la partie droite et gauche du sous tableau.

```c
void qsort_para(int l, int r) {
	if (r > l) {
		int pivot = a[r], tmp;

		int less = l - 1, more;

		for (more = l; more <= r; more++) {
			if (a[more] <= pivot) {
				less++;
				tmp = a[less];
				a[less] = a[more];
				a[more] = tmp;
			}
		}
		#pragma omp task default(none) firstprivate(l, less)
		{qsort_para(l, less - 1);}
		#pragma omp task default(none) firstprivate(r, less)
		{qsort_para(less + 1, r);}
		#pragma omp taskwait
	}
}
```



#### Cutoff

```c
void qsort_para(int l, int r, int cut) {
   if (r > l) {
      int pivot = a[r], tmp;

      int less = l - 1, more;

      for (more = l; more <= r; more++) {
         if (a[more] <= pivot) {
            less++;
            tmp = a[less];
            a[less] = a[more];
            a[more] = tmp;
         }
      }

      if (r - l  <= cut) {
         qsort_para(l, less - 1, cut);
         qsort_para(less + 1, r, cut);
      } else {
         #pragma omp task default(none) firstprivate(l, less) shared(cut)
         {qsort_para(l, less - 1, cut);}
         #pragma omp task default(none) firstprivate(r, less) shared(cut)
         {qsort_para(less + 1, r, cut);}
         #pragma omp taskwait
      }
   }
}
```



Ci dessous un graphique représenant différentes exécutions avec des valeurs de cutoff incrémentées.

![1570090951554](/home/pfontaine/.config/Typora/typora-user-images/1570090951554.png)

## Ensemble de Mandelbrot

### Resultats séquentiel

```bash
Calcul de l'ensemble de Mandelbrot 
----------------------------------

Largeur		: 1000
Hauteur		: 1000
Xmin		: -2.000000
Ymin		: -1.250000
CoteX		: 2.500000
CoteY		: 2.500000
Max iterations	: 50
omp Time seq = 0.09846500200364972
```

### Parallélisation

#### CalculImage

##### Sans Collapse

```c
void CalculImage() {
	int i,j;
	#pragma omp parallel default(none) private(i,j) shared(hauteur, largeur)
	{
		#pragma omp for
		for(i=0; i<hauteur; i++) {
			for(j=0; j<largeur; j++)
				CalculPoint(j,i);
		}
	}
}
```

```bash
Calcul de l'ensemble de Mandelbrot 
----------------------------------

Largeur		: 1000
Hauteur		: 1000
Xmin		: -2.000000
Ymin		: -1.250000
CoteX		: 2.500000
CoteY		: 2.500000
Max iterations	: 50
omp Time seq = 0.03654896598891355
```



##### Avec collapse

Ici nous avons de boucles parfaitement imbriqués, nous allons donc pouvoir sensiblement gagner en performance et indiquant au compilateur de transformer ce bloc en une unique boucle `for`.

```c
void CalculImage() {
	int i,j;
	#pragma omp parallel default(none) private(i,j) shared(hauteur, largeur)
	{
		#pragma omp for collapse(2)
		for(i=0; i<hauteur; i++) {
			for(j=0; j<largeur; j++)
				CalculPoint(j,i);
		}
	}
}
```

```bash
Calcul de l'ensemble de Mandelbrot 
----------------------------------

Largeur		: 1000
Hauteur		: 1000
Xmin		: -2.000000
Ymin		: -1.250000
CoteX		: 2.500000
CoteY		: 2.500000
Max iterations	: 50
omp Time seq = 0.02514284200151451
```

![1570044270146](/home/pfontaine/.config/Typora/typora-user-images/1570044270146.png)

#### Collapse et schedule static avec bunchs

```bash
void CalculImage() {
	int i,j;
	#pragma omp parallel default(none) private(i,j) shared(hauteur, largeur)
	{
		#pragma omp for collapse(2) schedule(static, hauteur/12)
		for(i=0; i<hauteur; i++) {
			for(j=0; j<largeur; j++)
				CalculPoint(j,i);
		}
	}
}
```

```bash
Calcul de l'ensemble de Mandelbrot 
----------------------------------

Largeur		: 1000
Hauteur		: 1000
Xmin		: -2.000000
Ymin		: -1.250000
CoteX		: 2.500000
CoteY		: 2.500000
Max iterations	: 50
omp Time seq = 0.01373629199224524
```

Nous observons ici que nous avons une accélération de :
$$
0.02514284200151451 / 0.01373629199224524 = 1.8303951325225747
$$
Ce qui est non négligeable.



## Conlusion

Ce que nous retiendrons à la suite de ces différents exercices sont les points suivants :

- Le séquentiel peut s'avérer plus efficaces sur certains traitements, les critères qui rentrent en compte sont la complexité de l'algorithme, la taille des données, le nombre d'instructions dans une boucle ou encore le nombre d'appels récursif à dépiler/empiler.
- Basé sur le point précédant, selon l'algorithme (d'autant plus si il est récursif), il peut être intéressant de mettre en place une valeur qui permettra de définir un seuil à partir duquel il sera plus intéressant de passer en séquentiel. Ainsi nous prenons les avantages des 2 versions et gagnons en performance. Ceci dit, trouver le seuil n'est pas trivial, il faut établir des tests qui permettront d'obtenir une valeur qui sera propre à la machine.
- Pour les boucles `for` imbriqués, il est souvent intéressant les transformer en une unique boucle. Ainsi il est possible d'utiliser les optimisations du compilateur.
- Pour les fonction récursif à multiples appels il est intéressant d'utiliser une combinaisons de `parallel` + `single` + `task`. Ainsi nous créons un pool de threads, la fonction sera appelée qu'une fois, mais les appels filles pourront être exécutées par ceux présents dans la team.

## Annexes

### Script Python pour stats

```python
# !python3
# coding: utf-8

import os
import sys
import re
import numpy as np
import matplotlib.pyplot as plt

# Function that parse the res and find the time
def parse_wtime(out_cmd):
    float_res = re.findall(r"[+-]?\d+\.\d*e?[+-]?\d*", out_cmd)

    if (float_res):
        return float(float_res[0])
    else:
        print "error : "
        print out_cmd

if __name__ == "__main__":

    sample = 100
    argv = [1e3, 1e4, 1e5, 1e6]
    ax_l = []
    fig, ax = plt.subplots()

    for i, arg in enumerate(argv):
        res_cmd1 = []
        res_cmd2 = []

        cmd1 = "./arrayMaj " + str(arg)
        cmd2 = "./arrayMajPar " + str(arg)

        for _ in range(sample):
            res_cmd1.append(parse_wtime(os.popen(cmd1).read()))

        for _ in range(sample):
            res_cmd2.append(parse_wtime(os.popen(cmd2).read()))

        if i == 0:
            ax_l.append(ax.bar(str(arg), np.mean(res_cmd1), label='non par', color=(0.2, 0.4, 0.6, 0.6)))
            ax_l.append(ax.bar(str(arg)+'_p', np.mean(res_cmd2), label='para', color=(0.2, 0.9, 0.6, 0.6)))
        else:
            ax_l.append(ax.bar(str(arg), np.mean(res_cmd1), color=(0.2, 0.4, 0.6, 0.6)))
            ax_l.append(ax.bar(str(arg)+'_p', np.mean(res_cmd2), color=(0.2, 0.9, 0.6, 0.6)))

    ax.set_title('comparaison boucle taille')
    ax.set_ylabel('temps execution moyen')
    plt.legend(loc='best')
    plt.show()
```


