/*****************************************************************************/
/*                                                                           */
/* Fonction d'ecriture d'un fichier tga a partir de 3 tableaux               */
/*****************************************************************************/

#include <stdio.h>

void SauvegardeTga(char rouge[], char vert[], char bleu[], int hauteur,
		int largeur, char* nom) {
	int h, l, i, j;
	FILE* Sortie;

	Sortie = fopen(nom, "w");

	/* ecriture de l'entete */
	for (i = 0; i < 12; i++) {
		if (i == 2)
			fprintf(Sortie, "%c", (char) 2);
		else
			fprintf(Sortie, "%c", (char) 0);
	}

	h = hauteur;
	l = largeur;

	/* ecriture des dimensions de l'image */

	fprintf(Sortie, "%c", (char) (l & 0xff));
	fprintf(Sortie, "%c", (char) ((l >> 8) & 0xff));
	fprintf(Sortie, "%c", (char) (h & 0xff));
	fprintf(Sortie, "%c", (char) ((h >> 8) & 0xff));
	fprintf(Sortie, "%c", (char) (24));
	fprintf(Sortie, "%c", (char) (0x20));

	for (i = 0; i < h; i++)
		for (j = 0; j < l; j++) {
			/* le repere est inverse en y */
			fprintf(Sortie, "%c", bleu[j + i * l]);
			fprintf(Sortie, "%c", vert[j + i * l]);
			fprintf(Sortie, "%c", rouge[j + i * l]);
		}
	fflush(Sortie);
	fclose(Sortie);
	printf("Fichier %s cree.\n", nom);
}

