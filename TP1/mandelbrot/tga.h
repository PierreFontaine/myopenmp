/*****************************************************************************/
/*                                                                           */
/* Calcul de l'ensemble de Mandelbrot                                        */
/*                                                                           */
/* declaration de la fonction definie dans tga.c                             */
/*****************************************************************************/

extern void SauvegardeTga(char rouge[], char vert[], char bleu[], int h, int l, char* nom) ;
