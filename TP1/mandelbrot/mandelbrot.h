/*****************************************************************************/
/*                                                                           */
/* Calcul de l'ensemble de Mandelbrot                                        */
/*                                                                           */
/* declaration des fonctions definies dans mandelbrot.c                      */
/*****************************************************************************/

extern void 	Initialise() ;
extern void 	CalculImage() ;
extern void 	CalculImagePar();
extern void 	CalculImageRegion() ;
extern void     CalculImageRegionCritique() ;
extern void	Sauvegarde() ;
extern void	SauvegardeIn(char * name);


