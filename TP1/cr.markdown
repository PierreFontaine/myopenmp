# Compte rendu de TP1

## Hello World

### Question 1

Ici nous déclarons une région parallèle avec une team de threads qui s'élevera au nombre de coeurs logique que nous possèdons sur notre ordinateur.
Pour ma part je dispose de 12 coeurs logiques.

Chacun de ces threads possèderont leurs propre copie de la variable `numThreads` et de la variable `tid`.

### Question 2

    Hello World from thread number 1
    Hello World from thread number 0
    Number of threads is 12
    Hello World from thread number 9
    Hello World from thread number 10
    Hello World from thread number 8
    Hello World from thread number 2
    [...]

