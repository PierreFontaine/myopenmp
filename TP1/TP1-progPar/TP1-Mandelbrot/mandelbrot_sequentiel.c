/*****************************************************************************/
/*                                                                           */
/* Calcul de l'ensemble de Mandelbrot                                        */
/*****************************************************************************/

#include <strings.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "tga.h"
#include <omp.h>

/*****************************************************************************/
/* Les differentes variables pour le calcul de l'ensemble		     */
/*****************************************************************************/

#define MAXX	1024
#define MAXY	1024

int maxiter, netb,
    iteration[MAXX][MAXY],
    hauteur, largeur, /* pour l'image */
    format; /* pour la sauvegarde */

float xmin, ymin, cotex, cotey;

double	pasx, pasy;

char chemin[150], /* le repertoire courant */
     nom[200]; /* le nom du fichier de sortie */

/*****************************************************************************/
/* Fonctions locales (pour les palettes de couleurs)			     */
/*****************************************************************************/

int RougeVert(int c) {
  if (c<128) return (256 -(2*c));
  else return 0 ;
}

int VertVert(int c) {
  return c ;
}

int BleuVert(int c) {
  return (256 -c);
}

int RougeRGB(int i, int mi) {
  int b = mi / 8 ;
  double d = 256.0 / b ;
  
  if (i==mi)
    return (0);
  else if (i < b)
    return ((int)(d*i));
  else if (i < 2*b)
    return (255 - (int)((i-b)*d));
  else if (i < 3*b)
    return (0x00);
  else if (i < 4*b)
    return ((int)((i-3*b)*d));
  else if (i < 7*b)
    return (255);
  else return (255 - (int)((i-7*b)*d));
}

int VertRGB(int i, int mi) {
  int b = mi / 8 ;
  double d = 256.0 / b ;

  if (i==mi)
    return (0);
  else if (i < b)
    return (0x00);
  else if (i < 2*b)
    return ((int)((i-b)*d));
  else if (i < 3*b)
    return (255 - (int)((i-2*b)*d));
  else if (i < 4*b)
    return ((int) ((i-3*b)*d));
  else if (i < 6*b)
    return (255 - (int)((i-5*b)*d/2));
  else return (0);
}

int BleuRGB(int i, int mi) {
  int b = mi / 8 ;
  double d = 256.0 / b ;

  if (i==mi)
    return (0);
  else if (i < b)
    return (255-(int)(i*d));
  else if (i < 2*b)
    return (0x00);
  else if (i < 3*b)
    return ((int)((i- 2*b)*d));
  else if (i < 4*b)
    return (255 -(int)((i-3*b)*d));
  else if (i < 6*b)
    return (0);
  else if (i < 7*b)
    return ((int)((i-6*b)*d));
  else return (255);
}

int RougeGris(int i) {
  static int r;
  if (i==0)
    r = 0 ;
  else if (i < 32)
    r = i*6+50 ;
  else if (i < 64)
    r = 255-6*(i-32) ;
  else if (i < 128)
    r = 50+3*(i-64) ;
  else if (i < 192)
    r = 256-3*(i-128) ;
  else if (i < 256)
    r = 50+3*(i-192) ;
  return r;
}

int VertGris(int i) {
  static int r;
  if (i==0)
    r = 0 ;
  else if (i < 32)
    r = i*6+50 ;
  else if (i < 64)
    r = 255-6*(i-32) ;
  else if (i < 128)
    r = 50+3*(i-64) ;
  else if (i < 192)
    r = 256-3*(i-128) ;
  else if (i < 256)
    r = 50+3*(i-192) ;
  return r;
}

int BleuGris(int i) {
  static int r;
  if (i==0)
    r = 0 ;
  else if (i < 32)
    r = i*6+50 ;
  else if (i < 64)
    r = 255-6*(i-32) ;
  else if (i < 128)
    r = 50+3*(i-64) ;
  else if (i < 192)
    r = 256-3*(i-128) ;
  else if (i < 256)
    r = 50+3*(i-192) ;
  return r;
}

int RougeNetB(int c, int s) {
  if (c < s)
    return (255);
  else
    return (0);
}

int VertNetB(int c, int s) {
  if (c < s)
    return (255);
  else
    return (0);
}

int BleuNetB(int c, int s) {
  if (c < s)
    return (255);
  else
    return (0);
}

/*****************************************************************************/
/* fonction Rouge : retourne la composante rouge du point (x,y)              */
/* suivant le modele de couleur choisi			      		     */
/*****************************************************************************/

int Rouge(int x, int y)	{
  static int c, r;
  c = iteration[x][y] ; 
  
  if (format==0)
    r = RougeRGB(c, maxiter);
  else if (format==1)
    r = RougeVert(c);
  else if (format==2)
    r = RougeGris(c);
  else if (format==3)
    r = RougeNetB(c, netb);
  return r ;
}

/*****************************************************************************/
/* fonction Vert : retourne la composante verte du point (x,y)               */
/* suivant le modele de couleur choisi			      		     */
/*****************************************************************************/

int Vert(int x, int y) {
  static int c, r;
  c =  iteration[x][y] ;
  
  if (format==0)
    r = VertRGB(c, maxiter);
  else if (format==1)
    r = VertVert(c);
  else if (format==2)
    r = VertGris(c);
  else if (format==3)
    r = VertNetB(c, netb);
  return r ;
}

/*****************************************************************************/
/* fonction Bleu : retourne la composante bleue du point (x,y)               */
/* suivant le modele de couleur choisi			      		     */
/*****************************************************************************/

int Bleu(int x, int y) {
  static int c, r;
  c = iteration[x][y] ;
  
  if (format==0)
    r = BleuRGB(c, maxiter);
  else if (format==1)
    r = BleuVert(c);
  else if (format==2)
    r = BleuGris(c);
  else if (format==3)
    r = BleuNetB(c, netb);
  return r ;
}

/*****************************************************************************/
/* fonction Initialise : demande a l'utilisateur les parametres necessaires  */
/* pour determiner l'ensemble de Mandelbrot		       		     */
/*****************************************************************************/

void Initialise() {
/*
  printf("Quelle l'abscisse du point en bas a gauche ?");
  scanf("%f", &xmin) ;
  printf("Quelle l'ordonnee du point en bas a gauche ?");
  scanf("%f", &ymin) ;
  printf("Quelle est la longueur sur l'axe des x du rectangle  ?");
  scanf("%f", &cotex) ;
  printf("Quelle est la longueur sur l'axe des y du rectangle  ?");
  scanf("%f", &cotey) ;
  printf("Quelle est la hauteur de l'image  ?");
  scanf("%d", &hauteur) ;
  printf("Quelle est la largeur de l'image  ?");
  scanf("%d", &largeur) ;
  printf("Nombre d'iterations autorisees  ?");
  scanf("%d", &maxiter) ;
  printf("Format (0 : RGB, 1 : vert, 2 : gris, 3: n&b) ?");
  scanf("%d", &format) ;
  if (format==3) {
    printf("Quel est le seuil ?");
    scanf("%d", &netb);
  }
*/

  xmin=-2;
  ymin=-1.25;
  cotex=2.5;
  cotey=2.5;
  hauteur=1000;
  largeur=1000;
  maxiter=50;
  format=0;

  pasx = (double) cotex / largeur ;
  pasy = (double) cotey / hauteur ;
	
  printf("Calcul de l'ensemble de Mandelbrot \n") ;
  printf("----------------------------------\n\n")  ;

  printf("Largeur		: %d\n", largeur) ;
  printf("Hauteur		: %d\n", hauteur) ;
  printf("Xmin		: %f\n", xmin) ;
  printf("Ymin		: %f\n", ymin) ;
  printf("CoteX		: %f\n", cotex) ;
  printf("CoteY		: %f\n", cotey) ;
  printf("Max iterations	: %d\n", maxiter) ;
  if (format==3) printf("Noir & blanc	: %d\n", netb) ;
}

/*****************************************************************************/
/* fonction CalculPoint : calcul le nombre d'iterations necessaires pour     */
/* determiner si le point ecran (cx, cy) appartient a l'ensemble de          */
/* Mandelbrot                                                                */
/*****************************************************************************/

void CalculPoint(int cx,int cy) {
  double sx, x=0, rx , y=0, ry , m=0 ;
  int 	 n=0 ;

  /* les coordonnees dans l'espace utilisateur */
  rx = (double) xmin + pasx * cx ; 
  ry = (double) ymin + pasy * cy ;

  while( (m<2) && (n<maxiter)) {
    sx = x ; /* pour le calcul de y */
    x = x*x - y*y + rx ;
    y = 2*sx*y + ry ;
    m = sqrt(x*x+y*y) ;
    n++ ;
  }
  iteration[cx][cy] = n ;
}


/*****************************************************************************/
/* fonction CalculImage : calcul pour chaque point P de l'ecran, le nombre   */
/* d'iterations necessaires pour determiner si P appartient a l'ensemble de  */
/* Mandelbrot                                                                */
/*****************************************************************************/

void CalculImage() {
   int i,j;

  { for(i=0; i<hauteur; i++)
    {
              for(j=0; j<largeur; j++)
              {          CalculPoint(j,i);
              }
    }
   }
}

/*****************************************************************************/
/* fonction Sauvegarde : cree un fichier image tga representant l'ensemble   */
/*  de mandelbrot calcule                                                    */
/*****************************************************************************/

void Sauvegarde() {
	SauvegardeIn("mandelbrot.tga");
}

void SauvegardeIn(char * name) {

	char nom[50];
	int i, j;

	char *r = (char *) malloc(sizeof(char) * (MAXX * MAXY));
	char *v = (char *) malloc(sizeof(char) * (MAXX * MAXY));
	char *b = (char *) malloc(sizeof(char) * (MAXX * MAXY));

		for (i = 0; i < hauteur; i++)
			for (j = 0; j < largeur; j++) {
				r[j + (hauteur - i - 1) * largeur] = Rouge(j, i);
				v[j + (hauteur - i - 1) * largeur] = Vert(j, i);
				b[j + (hauteur - i - 1) * largeur] = Bleu(j, i);
			}
	/* printf("Quel est le nom du fichier tga a creer ? ");
	 scanf("%s", &nom);
	 */
	strcpy(nom, name);
	SauvegardeTga(r, v, b, hauteur, largeur, nom);
}
