/*****************************************************************************/
/*                                                                           */
/* voici le programme principal...                                           */
/* c'est facile non ?                                                        */
/*****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include "mandelbrot.h"
#include <omp.h>
#include <math.h>

int main() {
	// pour le temps
	double start, end;

	Initialise(); /* initialisation des parametres */

	start = omp_get_wtime();
	CalculImage(); /* calcul de l'image */
	end = omp_get_wtime();

	printf("omp Time seq = %.16g\n", end - start);

	SauvegardeIn("Mandelbrot-seq.tga"); /* creation de l'image au format tga */

	return 0;
}
