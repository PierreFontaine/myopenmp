/*
 ============================================================================
 Name        : TP1-QSort.c
 Author      : km
 Version     :
 Copyright   : For education purpose only
 Description : Serial implementation of the QuickSort algorithm
 TO BE parallelized with OpenMP...
 ============================================================================
 */

#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include<time.h>

#define ARRAY_SIZE 10000009

int a[ARRAY_SIZE];

void qsort_serial(int l, int r) {
	if (r > l) {
		int pivot = a[r], tmp;
		int less = l - 1, more;
		for (more = l; more <= r; more++) {
			if (a[more] <= pivot) {
				less++;
				tmp = a[less];
				a[less] = a[more];
				a[more] = tmp;
			}
		}
		qsort_serial(l, less - 1);
		qsort_serial(less + 1, r);
	}
}

int main() {
	int n, i;
	//scanf("%d",&n);
	n = ARRAY_SIZE - 9;
	int range = 10000;
	srand(time(NULL));
	for (i = 0; i < n; i++)
		a[i] = rand() % range;
	printf("Starting quick sort...\n");
	double start = omp_get_wtime();
	qsort_serial(0, n - 1);
	double end = omp_get_wtime();
	printf("End QuickSort\n");
	printf("Time seq = %.16g for quicksort on array size = %d\n", end - start, ARRAY_SIZE);
	return 0;
}
