#include <omp.h>
#include <iostream>

using namespace std;

int main(int argc, char *argv[]) {
	int numThreads, tid;

#pragma omp parallel private(numThreads, tid)
	{
		tid = omp_get_thread_num();
		cout << "Hello World from thread number " <<  tid << endl;

		if (tid == 0) {
			numThreads = omp_get_num_threads();
			cout << "Number of threads is " << numThreads << endl;
		}
	}

	return 0;
}
