//
// Created by pfontaine on 30/09/2019.
//

#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include<time.h>

#define ARRAY_SIZE 10000009

int a[ARRAY_SIZE];

void printArray() {
	for (int i = 0; i < 10; ++i) {
		printf("[%d] %d\n",i,a[i]);
	}

	for (int i = 10000; i < 10000 + 10; ++i) {
		printf("[%d] %d\n",i, a[i]);
	}
}

void qsort_para(int l, int r, int cut) {
	if (r > l) {
		int pivot = a[r], tmp;

		int less = l - 1, more;

		for (more = l; more <= r; more++) {
			if (a[more] <= pivot) {
				less++;
				tmp = a[less];
				a[less] = a[more];
				a[more] = tmp;
			}
		}

		if (r - l  <= cut) {
			qsort_para(l, less - 1, cut);
			qsort_para(less + 1, r, cut);
		} else {
			#pragma omp task default(none) firstprivate(l, less) shared(cut)
			{qsort_para(l, less - 1, cut);}
			#pragma omp task default(none) firstprivate(r, less) shared(cut)
			{qsort_para(less + 1, r, cut);}
			#pragma omp taskwait
		}
	}
}


/**
 * @brief main function calling the quicksort
 * @return 0 if ok
 */
int main(int argc, char *argv[]) {
	int n, i;
	//scanf("%d",&n);
	n = ARRAY_SIZE - 9;
	int range = 10000;

	if (argc != 2) {
		fprintf( stdout, "usage:  %s  cuttof\n", argv[0]);
		exit(-1);
	}

	srand(time(NULL));
	for (i = 0; i < n; i++)
		a[i] = rand() % range;
	printf("Starting quick sort...\n");
	double start = omp_get_wtime();

	#pragma omp parallel default(none) shared(a,n, argv)
	{
		#pragma omp single nowait
		{
			qsort_para(0, n - 1,(int) atof(argv[1]));
		}
	}
	double end = omp_get_wtime();
	printf("End QuickSort\n");
	printf("Time seq = %.16g for quicksort on array size = %d\n", end - start, ARRAY_SIZE);
	printArray();
	return 0;
}