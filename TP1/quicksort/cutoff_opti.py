# !python3
# coding: utf-8

import os
import re
import numpy as np
import matplotlib.pyplot as plt


# Function that parse the res and find the time
def parse_wtime(out_cmd):
    float_res = re.findall(r"[+-]?\d+\.\d*e?[+-]?\d*", out_cmd)

    if (float_res):
        return float(float_res[0])
    else:
        print("error : ")
        print(out_cmd)


if __name__ == "__main__":
    sample = 1

    argv = range(0, 5000, 100)
    res = []

    for arg in argv:
        print("[-] Executing " + str(arg))
        cmd = "./cmake-build-debug/quicksort_par_co " + str(arg)
        res_raw = []
        for _ in range(sample):
            res_raw.append(parse_wtime(os.popen(cmd).read()))

        res.append(np.mean(res_raw))

    plt.xlabel("Cut off valeur")
    plt.ylabel("Temps")
    plt.title("Cut Off optimisation")
    plt.scatter(argv, res)
    print(np.min(res))
    # plt.plot([np.min(res), np.min(res)], [0, 35])
    plt.plot(argv, res)
    plt.show()
