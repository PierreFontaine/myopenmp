/**  Fichier arrayMaj.c            */
/*
 * Modifications
 * Kevin : use of omp_get_wtime 2016/09/27
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <omp.h>

int main(int argc, char *argv[]) {
	int para = 1;
	double *x, *y;
	int arraySize;
	int i;

	if (argc != 2) {
		fprintf( stdout, "usage:  %s  arraySize\n", argv[0]);
		exit(-1);
	}

	arraySize = (int) atof(argv[1]);

	printf("Taille du tableau = %d\n\n", arraySize);

	x = (double *) malloc((size_t) (arraySize * sizeof(double)));
	y = (double *) malloc((size_t) (arraySize * sizeof(double)));

	/* Initialisation de x */
	for (i = 0; i < arraySize; i++) {
		x[i] = ((double) i) / (i + 1000);
	}

	/* Ici commence les traitements que nous souhaitons paralléliser */
	/* Lancement du chronomètre */

	printf("Processing para...\n");
	double start = omp_get_wtime();

#pragma omp parallel default(none) shared(arraySize,x,y)
	{
#pragma omp for private(i)
		for (i = 0; i < arraySize; i++) {
			y[i] = sin(exp(cos(-exp(sin(x[i])))));
		}
	}

	double end = omp_get_wtime();

	/* Affichage des données temporelles */
	fprintf( stdout, "\nwall time = %.16g sec for array size = %d\n", end - start,
			arraySize);

	fprintf( stdout, "\nProgram successfully terminated.\n\n");
	return (0);
}
